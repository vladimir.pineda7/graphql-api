# pylint: disable=C0114
import setuptools

with open('README.md', 'r') as fh:
    long_description = fh.read()

setuptools.setup(
    name='Kumpel GraphQL API',
    version='0.1.0',
    author='Teamspartans',
    author_email='author@example.com',
    description='Kumpel is a web application to search and rent rooms.',
    url='https://gitlab.com/teamspartans/rommie/',
    packages=setuptools.find_packages(),
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: GNU GENERAL PUBLIC LICENSE',
        'Operating System :: OS Independent',
    ],
    python_requires='>=3.6',
)
