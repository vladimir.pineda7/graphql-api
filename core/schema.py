"""Main Schema."""
import graphene

from kumpel_api.schema import Mutation as SchemeMutation
from kumpel_api.schema import Query as SchemaQuery


# pylint: disable=too-few-public-methods
class Query(SchemaQuery, graphene.ObjectType):
    """The root of the query for Kumpel API.

    Args:
        Query (Query): Kumpel application queries.
        graphene (ObjectType): GraphQL types.
    """

    # pylint: disable=unnecessary-pass
    pass


class Mutation(SchemeMutation, graphene.ObjectType):
    """Root scheme for Mutations of Kumpel API.

    Args:
        SchemeMutation (Mutation): Kumpel aplication Mutations
        graphene (ObjectType): GraphQL types.
    """

    # pylint: disable=unnecessary-pass
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
