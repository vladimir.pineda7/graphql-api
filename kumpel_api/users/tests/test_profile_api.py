"""API Users and Profiles Tests."""
from graphene_django.utils.testing import GraphQLTestCase

from core.schema import schema
from kumpel_api.users.models import Hobby
from kumpel_api.users.models import Interest
from kumpel_api.users.models.genres import Genre
from kumpel_api.users.models.profiles import Profile
from kumpel_api.users.models.users import User

# pylint: disable=too-few-public-methods


class ProfileTestCase(GraphQLTestCase):
    """Profile Tests API."""

    GRAPHQL_SCHEMA = schema
    GRAPHQL_URL = '/graphql'

    def setUp(self):  # noqa: D401
        """Setup for tests."""
        # pylint: disable=C0103
        self._user = User.objects.create(
            email='Chesley55@yahoo.com',
            first_name='Kip',
            last_name='McGlynn',
            phone_number='830-2245-3850',
            password='7f03cfeb2-7cc0-49a5-803b-053be1086282',
        )

        self._genre = Genre.objects.create(name='Male')
        self._hobby = Hobby.objects.create(name='hic')
        self._interest = Interest.objects.create(name='ipsum')

        self._profile = Profile.objects.create(
            user_id=self._user.id, genre_id=self._genre.id,
        )

    # Need a header
    # def test_work_profile_with_users_query(self):
    #     """Verify that query for get a profile works."""
    #     response = self.query(
    #         '''
    #         query {
    #             user{
    #                 id
    #                 dateJoined
    #                 profile{
    #                 id
    #                 picture
    #                 birthday
    #                 }
    #             }
    #         }
    #         '''
    #     )
    #     self.assertResponseNoErrors(response)

    # def test_work_edit_profile(self):
    #     """Verify that mutation for edit a profile work."""
    #     query = f"""
    #         mutation {{
    #             profileEdit(profileId: {self._profile.id}, firstName: "Carla",
    #             lastName: "Ramirez", email: "carla@hotmail.com",
    #             genreId: {self._genre.id}, password: "1234", phoneNumber: "+571245875",
    #             interests: [{self._interest.id}], hobbies: [{self._hobby.id}]) {{
    #                 profile{{
    #                     id
    #                     birthday
    #                     picture
    #                     user{{
    #                         email
    #                         firstName
    #                         lastName
    #                     }}
    #                 }}
    #             }}
    #         }}
    #         """
    #     response = self.query(query)
    #     self.assertResponseNoErrors(response)
