"""Genres for Users."""
from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE

from kumpel_api.utils.models import KumpelBaseModel

# pylint: disable=too-few-public-methods


class Genre(KumpelBaseModel, SafeDeleteModel):
    """Genre for users."""

    _safedelete_policy = SOFT_DELETE

    name = models.CharField(
        max_length=50,
        blank=False,
        null=False,
        unique=True,
        help_text='Name of the Genre example: Female, Male',
    )

    def __str__(self):
        """Return genre name."""
        return self.name

    class Meta(KumpelBaseModel.Meta):
        """Meta options for Genre."""

        db_table = 'GENRES'
