"""Tests for Providers."""
# pylint: disable=R0801
from django.contrib.auth.models import Group
from django.test import TestCase

from kumpel_api.rooms.models import Status
from kumpel_api.rooms.models.feature import Feature
from kumpel_api.rooms.models.feature_category import FeatureCategory
from kumpel_api.rooms.models.places_category import PlacesCategory
from kumpel_api.users.models import Genre
from kumpel_api.users.models import Hobby
from kumpel_api.users.models import Interest
from kumpel_api.users.models import Profile
from kumpel_api.users.models import User
from kumpel_api.utils.utils import DataGenerator


class UserProviderTestCase(TestCase):
    """User provider Tests."""

    def setUp(self):
        """Test setUp."""
        # pylint: disable=C0103
        self._generator = DataGenerator()

        self._user = User.objects.create(
            email=self._generator.fake.email(),
            username=self._generator.fake.name(),
            first_name=self._generator.fake.first_name(),
            last_name=self._generator.fake.last_name(),
            phone_number=self._generator.fake.bothify(text='+##############'),
            is_verified=True,
            is_client=True,
            password=self._generator.fake.md5(),
        )

    def test_get_fake_user_id(self):
        """Test for get a fake user id."""
        fake_user_id = self._generator.fake.user()
        self.assertIsNotNone(fake_user_id)

    def test_create_fake_user(self):
        """Get a random user created."""
        user = User.objects.create(
            email=self._generator.fake.email(),
            username=self._generator.fake.name(),
            first_name=self._generator.fake.first_name(),
            last_name=self._generator.fake.last_name(),
            phone_number=self._generator.fake.bothify(text='+##############'),
            is_verified=True,
            is_client=True,
            password=self._generator.fake.md5(),
        )

        self.assertIsNotNone(user)


class GenreProviderTestCase(TestCase):
    """Genre provider Tests."""

    def setUp(self):
        """Test setUp."""
        # pylint: disable=C0103
        self._generator = DataGenerator()

        self._genre = Genre.objects.create(
            name=self._generator.fake.simple_profile()['sex'],
        )

    def test_get_fake_genre_id(self):
        """Test for get a fake genre id."""
        fake_genre_id = self._generator.fake.genre_kumpel()
        self.assertIsNotNone(fake_genre_id)

    def test_create_fake_genre(self):
        """Get a random genre created."""
        genre = Genre.objects.create(name='redundant')

        self.assertIsNotNone(genre)


class HobbyProviderTestCase(TestCase):
    """Hobby provider Tests."""

    def setUp(self):
        """Test setUp."""
        # pylint: disable=C0103
        self._generator = DataGenerator()

        self._hobby = Hobby.objects.create(name='strategize',)

    def test_get_fake_hobby_id(self):
        """Test for get a fake hobby id."""
        fake_hobby = self._generator.fake.hobbies(hobbies_number=1)
        self.assertIsNotNone(fake_hobby)

    def test_create_fake_hobby(self):
        """Get a random genre created."""
        hobby = Hobby.objects.create(name='maximize')

        self.assertIsNotNone(hobby)


class InterestProviderTestCase(TestCase):
    """Interest provider Tests."""

    def setUp(self):
        """Test setUp."""
        # pylint: disable=C0103
        self._generator = DataGenerator()

        self._interests = Interest.objects.create(name='Direct',)

    def test_get_fake_interest_id(self):
        """Test for get a fake interest id."""
        fake_interest = self._generator.fake.interest(interests_number=1)
        self.assertIsNotNone(fake_interest)

    def test_create_fake_interest(self):
        """Get a random interest created."""
        interest = Interest.objects.create(name='circuit')

        self.assertIsNotNone(interest)


class ProfilesProviderTestCase(TestCase):
    """Profile Provider Tests."""

    def setUp(self):
        """Test setUp."""
        # pylint: disable=C0103
        self._generator = DataGenerator()

        self._user = User.objects.create(
            email=self._generator.fake.email(),
            username=self._generator.fake.name(),
            first_name=self._generator.fake.first_name(),
            last_name=self._generator.fake.last_name(),
            phone_number=self._generator.fake.bothify(text='+##############'),
            is_verified=True,
            is_client=True,
            password=self._generator.fake.md5(),
        )

        self._genre = Genre.objects.create(
            name=self._generator.fake.simple_profile()['sex'],
        )

        self._profiles = Profile.objects.create(
            user_id=self._user.id, genre_id=self._genre.id,
        )

    def test_get_fake_profile_id(self):
        """Test for get a fake profile id."""
        fake_profile_id = self._generator.fake.kumpel_profile()
        self.assertIsNotNone(fake_profile_id)

    def test_create_fake_profile(self):
        """Get a random profile created."""
        user = User.objects.create(
            email=self._generator.fake.email(),
            username=self._generator.fake.name(),
            first_name=self._generator.fake.first_name(),
            last_name=self._generator.fake.last_name(),
            phone_number=self._generator.fake.bothify(text='+##############'),
            is_verified=True,
            is_client=True,
            password=self._generator.fake.md5(),
        )

        profile = Profile.objects.create(
            user_id=user.id, genre_id=self._genre.id,
        )

        self.assertIsNotNone(profile)


class StatusProviderTestCase(TestCase):
    """Status Provider Tests."""

    def setUp(self):
        """Test setUp."""
        # pylint: disable=C0103
        self._generator = DataGenerator()

        self._status = Status.objects.create(status='protocol')

    def test_get_fake_status_id(self):
        """Test for get a fake status id."""
        fake_status_id = self._generator.fake.status()
        self.assertIsNotNone(fake_status_id)

    def test_create_fake_status(self):
        """Get a random status created."""
        status = Status.objects.create(status='Berkshire')

        self.assertIsNotNone(status)


class FeatureCategoryProviderTestCase(TestCase):
    """Feature Category Provider Tests."""

    def setUp(self):
        """Test setUp."""
        # pylint: disable=C0103
        self._generator = DataGenerator()

        self._feature_categories = FeatureCategory.objects.create(
            name='Feature Category'
        )

    def test_get_fake_feature_category_id(self):
        """Test for get a fake feature category id."""
        fake_feature_category_id = self._generator.fake.feature_category()
        self.assertIsNotNone(fake_feature_category_id)

    def test_create_fake_feature_category(self):
        """Get a random feature created."""
        feature_category = FeatureCategory.objects.create(
            name='Feature Category 2'
        )

        self.assertIsNotNone(feature_category)


class FeatureProviderTestCase(TestCase):
    """Feature Provider Tests."""

    def setUp(self):
        """Test setUp."""
        # pylint: disable=C0103
        self._generator = DataGenerator()

        self.feature = Feature.objects.create(name='circuit', is_boolean=True)

    def test_get_fake_feature(self):
        """Test for get a fake feature id."""
        feature = self._generator.fake.features(features_number=1)
        self.assertIsNotNone(feature)

    def test_create_fake_feature(self):
        """Get a random feature created."""
        feature = Feature.objects.create(name='bandwidth', is_boolean=False)

        self.assertIsNotNone(feature)


class PlaceCategoryProviderTestCase(TestCase):
    """Place Category Provider Tests."""

    def setUp(self):
        """Test setUp."""
        # pylint: disable=C0103
        self._generator = DataGenerator()

        self.place_category = PlacesCategory.objects.create(name='circuit')

    def test_get_fake_place_category(self):
        """Test for get a fake place category id."""
        place_category = self._generator.fake.place_category()
        self.assertIsNotNone(place_category)

    def test_create_fake_place_category(self):
        """Get a random category created."""
        place_category = PlacesCategory.objects.create(name='bandwidth')

        self.assertIsNotNone(place_category)


class GroupProviderTestCase(TestCase):
    """Group Provider Tests."""

    def setUp(self):
        """Test setUp."""
        # pylint: disable=C0103
        self._generator = DataGenerator()

        self.group = Group.objects.create(name='non')

    def test_get_fake_place_category(self):
        """Test for get a fake group."""
        group = self._generator.fake.group()
        self.assertIsNotNone(group)

    def test_create_fake_group(self):
        """Get a random category created."""
        group = Group.objects.create(name='bandwidth')

        self.assertIsNotNone(group)
