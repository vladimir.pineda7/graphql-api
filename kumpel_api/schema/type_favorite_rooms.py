"""Favorite Room types.

All favorite room Types for use on Querys and Mutations.
"""
import graphene
from graphene_django.types import DjangoObjectType

from kumpel_api.rooms.models.favorite_rooms import FavoriteRoom
from kumpel_api.schema.type_room import RoomType
from kumpel_api.schema.type_user import UserType

# pylint: disable=too-few-public-methods
# pylint: disable=no-self-argument
# pylint: disable=no-self-use
# pylint: disable=unused-argument
# pylint: disable=no-member


##
# Types
##
class FavoriteRoomType(DjangoObjectType):
    """Favorite Room type."""

    user = graphene.Field(UserType)
    room = graphene.Field(RoomType)

    def resolve_user(self, args):
        """Get user info."""
        return self.user

    def resolve_room(self, args):
        """Get user info."""
        return self.room

    class Meta:
        """Meta properties for the Type."""

        model = FavoriteRoom
        description = 'Rooms added to Favorite by a users.'


class FavoriteRoomPaginatedType(graphene.ObjectType):
    """Type for paginate Rooms."""

    page = graphene.Int()
    pages = graphene.Int()
    has_next = graphene.Boolean()
    has_prev = graphene.Boolean()
    data = graphene.List(FavoriteRoomType)

    class Meta:
        """Properties for the type."""

        model = FavoriteRoom
        description = 'Paginate Favorites Rooms.'


##
# Mutations
##
class CreateFavoriteRoom(graphene.Mutation):
    """Mutation for create a Favorite Room."""

    class Arguments:
        """Arguments for create a Favorite Room."""

        user_id = graphene.Int(required=True)
        room_id = graphene.Int(required=True)

    favorite_room = graphene.Field(FavoriteRoomType)

    def mutate(root, info, **kwargs):
        """Mutation method."""
        user = kwargs.get('user_id', None)
        room = kwargs.get('room_id', None)

        favorite_obj = FavoriteRoom.objects.create(user_id=user, room_id=room)
        return CreateFavoriteRoom(favorite_room=favorite_obj)
