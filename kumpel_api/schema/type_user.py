"""User types.

All user Types for use on Querys and Mutations.
"""
import graphene
import graphql_jwt
from django.contrib.auth.models import Group
from graphene_django.types import DjangoObjectType
from graphene_file_upload.scalars import Upload

from kumpel_api.users.models import Hobby
from kumpel_api.users.models import Interest
from kumpel_api.users.models import Profile
from kumpel_api.users.models import User

# pylint: disable=too-few-public-methods
# pylint: disable=no-self-argument
# pylint: disable=no-self-use
# pylint: disable=unused-argument
# pylint: disable=no-member

# Types


class KumpelRoleType(DjangoObjectType):
    """User Profile."""

    class Meta:
        """Meta Information for ProfileType."""

        model = Group
        description = 'Kumpel roles'
        only_fields = (
            'id',
            'name',
        )


class InterestType(DjangoObjectType):
    """Profile interest Type."""

    class Meta:
        """Meta Information for InterestType."""

        model = Interest
        description = 'Interests of the User'


class HobbyType(DjangoObjectType):
    """Hobby Interest Type."""

    class Meta:
        """Meta options for HobbyType."""

        model = Hobby
        description = 'Hbobies of the user'


class ProfileType(DjangoObjectType):
    """User Profile."""

    hobbies = graphene.List(HobbyType, description='User hobbies')

    interests = graphene.List(InterestType, description='User interests')

    def resolve_interests(self, args):
        """Get Interest of the user."""
        return self.interests.all()

    def resolve_hobbies(self, args):
        """Get Hobbies of the user."""
        return self.hobbies.all()

    class Meta:
        """Meta Information for ProfileType."""

        model = Profile
        description = 'User Profile'
        only_fields = (
            'id',
            'picture',
            'birthday',
        )


class UserType(DjangoObjectType):
    """User Type for Kumpel_Api."""

    full_name = graphene.Field(
        graphene.String,
        description='Get full name of user join first name and last name',
    )

    date_joined = graphene.Field(
        graphene.DateTime, description='Day user joined on platform'
    )

    group = graphene.List(KumpelRoleType, description="Role's user")

    profile = graphene.Field(
        ProfileType, description='Complementary data of User'
    )

    def resolve_full_name(self, args):
        """Get user info."""
        return self.get_full_name()

    def resolve_date_joined(self, args):
        """Get date user joined."""
        return self.date_joined

    def resolve_group(self, args):
        """Get group of users."""
        return self.groups.all()

    def resolve_profile(self, args):
        """Get group of users."""
        return self.profile

    class Meta:
        """Meta Information for ProfileType."""

        model = User
        description = 'User Profile'
        only_fields = (
            'id',
            'full_name',
            'date_joined',
            'first_name',
            'last_name',
            'email',
            'phone_number',
            'group',
            'profile',
        )


class KumpelLogin(graphql_jwt.ObtainJSONWebToken):
    """Login API."""

    user = graphene.Field(UserType)

    @classmethod
    def resolve(cls, root, info, **kwargs):
        """Add User login details."""
        return cls(user=info.context.user)


class KumpelRefreshToken(graphql_jwt.ObtainJSONWebToken):
    """Login API."""

    user = graphene.Field(UserType)

    @classmethod
    def resolve(cls, root, info, **kwargs):
        """Add User login details."""
        return cls(user=info.context.user)


class KumpelLogout(graphql_jwt.Revoke):
    """Logout."""

    @classmethod
    def resolve(cls, root, info, **kwargs):
        """Revoke user session."""
        return cls()


# Mutations
class KumpelRegister(graphene.Mutation):
    """Mutation for create a user."""

    class Arguments:
        """Create an account."""

        first_name = graphene.String(required=True)
        last_name = graphene.String(required=True)
        email = graphene.String(required=True)
        password = graphene.String(required=True)
        role = graphene.ID(required=True)

    user = graphene.Field(UserType)

    def mutate(root, info, role, **kwargs):
        """Mutation method."""
        first_name = kwargs.get('first_name', None)
        last_name = kwargs.get('last_name', None)
        email = kwargs.get('email', None)
        password = kwargs.get('password', None)

        group = Group.objects.get(id=role)
        if group.name != 'Spartans':
            user_obj = User(
                first_name=first_name, last_name=last_name, email=email,
            )
            user_obj.set_password(password)
            user_obj.save()
            user_obj.groups.add(group)

            profile = Profile(user_id=user_obj.id, genre_id=3)
            profile.save()
        else:
            raise Exception('Invalid data for create a kumpel account!')

        return KumpelRegister(user=user_obj)


class EditProfileMutation(graphene.Mutation):
    """Mutation for create a Favorite Room."""

    class Arguments:
        """Arguments for create a Favorite Room."""

        profile_id = graphene.Int(required=True)
        picture = Upload(required=False)
        genre_id = graphene.Int(required=False)
        hobbies = graphene.List(graphene.Int, required=False)
        interests = graphene.List(graphene.Int, required=False)
        email = graphene.String(required=False)
        password = graphene.String(required=False)
        first_name = graphene.String(required=False)
        last_name = graphene.String(required=False)
        phone_number = graphene.String(required=False)
        birthdate = graphene.String(required=False)

    profile = graphene.Field(ProfileType)

    def mutate(root, info, **kwargs):
        """Mutation method."""
        # pylint: disable=too-many-locals,too-many-branches
        profile_id = kwargs.get('profile_id', None)
        genre = kwargs.get('genre_id', None)
        hobbies = kwargs.get('hobbies', None)
        interests = kwargs.get('interests', None)
        password = kwargs.get('password', None)
        first_name = kwargs.get('first_name', None)
        last_name = kwargs.get('last_name', None)
        phone_number = kwargs.get('phone_number', None)
        email = kwargs.get('email', None)
        birthdate = kwargs.get('birthdate', None)
        birthdate = kwargs.get('birthdate', None)
        picture = kwargs.get('picture', None)
        profile_obj = Profile.objects.get(pk=profile_id)
        if genre:
            profile_obj.genre_id = genre
        if hobbies:
            hobbies_set = []
            for hobby_id in hobbies:
                hobby = Hobby.objects.get(pk=hobby_id)
                hobbies_set.append(hobby)
            profile_obj.hobbies.set(hobbies)
        if interests:
            interests_set = []
            for interest_id in interests:
                interest = Interest.objects.get(pk=interest_id)
                interests_set.append(interest)
            profile_obj.interests.set(interests_set)
        if password:
            profile_obj.user.set_password(password)
        if first_name:
            profile_obj.user.first_name = first_name
        if last_name:
            profile_obj.user.last_name = last_name
        if phone_number:
            profile_obj.user.phone_number = phone_number
        if email:
            profile_obj.user.email = email
        if birthdate:
            profile_obj.user.birthday = birthdate
        if picture:
            picture_profile = picture
            profile_obj.picture = picture_profile

        profile_obj.user.save()
        profile_obj.save()

        return EditProfileMutation(profile=profile_obj)
