"""Create initial and faker data."""
import datetime
import json
import logging
import os
from io import BytesIO
from random import choice
from random import randint

import requests
from django.apps import apps
from django.contrib.auth.models import Group
from django.core.files.base import ContentFile
from django.db import connection
from django.db import IntegrityError
from PIL import Image

from kumpel_api.utils.utils import DataGenerator


# pylint: disable=C0103
# pylint: disable=W1309
# pylint: disable=C0301

##
# Load Data
##
def load_cities_json():
    """Load cities data."""
    data = None
    with open('Notebooks/Datasets/states_cities.json') as json_file:

        data = json.load(json_file)

    return data


CITIES_JSON = load_cities_json()


def get_random_city():
    """Get random city."""
    state, cities = choice(list(CITIES_JSON.items()))
    city, settlements = choice(list(cities.items()))
    settlement = choice(settlements)
    return state + ', ' + city + ', ' + settlement


##
# Catalogs
##
def generate_catalogs():
    """Generate all catalog data."""
    generate_features_categories()
    generate_places_categories()
    generate_status()
    generate_features()
    create_groups()
    create_genres()
    create_interests()
    create_hobbies()
    return True


def clear_room_models():
    """Clear room models."""
    cursor = connection.cursor()
    cursor.execute('TRUNCATE TABLE "ROOMS" CASCADE')
    cursor.execute('TRUNCATE TABLE "ROOMS_PLACES" CASCADE')
    cursor.execute('TRUNCATE TABLE "ROOMS_IMAGES" CASCADE')
    cursor.execute('TRUNCATE TABLE "ROOMS_FEATURES" CASCADE')
    cursor.execute('TRUNCATE TABLE "FAVORITE_ROOMS"')
    return True


def clear_user_models():
    """Clear users models."""
    cursor = connection.cursor()
    cursor.execute('TRUNCATE TABLE "refresh_token_refreshtoken" CASCADE')
    cursor.execute('TRUNCATE TABLE "USERS_HOBBIES" CASCADE')
    cursor.execute('TRUNCATE TABLE "USERS_INTERESTS" CASCADE')
    cursor.execute('TRUNCATE TABLE "USERS_PROFILES" CASCADE')
    cursor.execute('TRUNCATE TABLE "USERS_user_permissions"')
    cursor.execute(
        'DELETE FROM "auth_group_permissions" where group_id in (1,2)'
    )
    cursor.execute('DELETE FROM "USERS_groups" where group_id in (1,2)')
    text_filter = "'esteban.ladino.927@gmail.com', 'campuxano88@gmail.com', "
    text_filter2 = (
        "'dimple.pleod@gmail.com', 'cristhianeduardo03@hotmail.com', "
    )
    text_filter3 = (
        "'estefanyaguilar.r@gmail.com', 'eliasojedamedina@gmail.com'"
    )
    cursor.execute(
        f'DELETE FROM "USERS" WHERE EMAIL NOT IN ({text_filter}{text_filter2}{text_filter3});'
    )
    return True


def generate_users_fake_data(n):
    """Generate fake data."""
    create_users(n)
    return True


def generate_rooms_fake_data():
    """Generate fake data."""
    generate_rooms()
    generate_favorite_rooms()
    return True


##
# Rooms
##
def generate_features_categories():
    """Generate Feature  Categories."""
    FeatureCategory = apps.get_model('rooms', 'FeatureCategory')
    FeatureCategory.objects.update_or_create(id=1, name='The Rooms')
    FeatureCategory.objects.update_or_create(id=2, name='The House')
    return True


def generate_places_categories():
    """Generate Places Category."""
    PlacesCategory = apps.get_model('rooms', 'PlacesCategory')
    PlacesCategory.objects.update_or_create(id=1, name='Hospital')
    PlacesCategory.objects.update_or_create(id=2, name='Parks')
    PlacesCategory.objects.update_or_create(id=3, name='Bank')
    PlacesCategory.objects.update_or_create(id=4, name='University')
    PlacesCategory.objects.update_or_create(id=5, name='Gass Station')
    return True


def generate_status():
    """Generate Rooms Status."""
    Status = apps.get_model('rooms', 'Status')
    Status.objects.update_or_create(id=1, status='Available')
    Status.objects.update_or_create(id=2, status='Taken')
    return True


def generate_users_rooms(user, rooms_number):
    """Generate users rooms."""
    Room = apps.get_model('rooms', 'Room')
    generator = DataGenerator()
    generator = generator.fake
    city = get_random_city()

    for _ in range(rooms_number):
        coordinates = generator.latlng()
        room = Room.objects.create(
            title=generator.text(max_nb_chars=80),
            description=generator.text(max_nb_chars=200),
            host_id=user.id,
            status_id=generator.status(),
            price=generator.random_number(digits=3),
            address=city + ', ' + generator.address(),
            latitude=coordinates[0],
            longitude=coordinates[1],
            visibility=True,
            availability_date=generator.date_between(
                start_date='today', end_date=datetime.date(2020, 8, 7)
            ),
        )
        room.features.set(generator.features(features_number=randint(1, 5)))
        generate_room_images(room, main=True)
        generate_room_images(room, main=False, images_number=randint(2, 8))
        generate_places(room, places_number=randint(0, 10))

        date = generator.date_between(
            start_date=datetime.date(2020, 1, 1), end_date='today'
        )
        room.created = date
        room.modified = date
        room.save()


def generate_room_images(room, main=False, images_number=5):
    """Generate images for a room."""
    if main:
        try:
            response = requests.get(
                'https://source.unsplash.com/1600x900/?rooms'
            )
            main_image = Image.open(BytesIO(response.content)).convert('RGB')

            avatar_io = BytesIO()

            main_image.save(avatar_io, format='JPEG', quality=70)

            thumb_file = ContentFile(avatar_io.getvalue())

            room.main_image.save(
                os.path.basename(room.title + '.jpg'), thumb_file
            )
        except requests.exceptions.ConnectionError as error:
            logging.exception(error)
    else:
        try:
            RoomImage = apps.get_model('rooms', 'RoomImage')
            for _ in range(images_number):
                response = requests.get(
                    'https://source.unsplash.com/1600x900/?rooms'
                )

                main_image = Image.open(BytesIO(response.content)).convert(
                    'RGB'
                )

                avatar_io = BytesIO()

                main_image.save(avatar_io, format='JPEG', quality=70)

                thumb_file = ContentFile(avatar_io.getvalue())
                room_image = RoomImage(room=room)
                room_image.image.save(
                    os.path.basename(room.title + '.jpg'), thumb_file
                )
                room_image.save()
        except requests.exceptions.ConnectionError as error:
            logging.exception(error)
    return True


def generate_rooms():
    """Generate Rooms."""
    User = apps.get_model('users', 'User')

    for user in User.objects.filter(groups__name='Host'):
        generate_users_rooms(user, randint(1, 6))
    return True


def generate_features():
    """Generate Feature Category."""
    Feature = apps.get_model('rooms', 'Feature')
    Feature.objects.update_or_create(
        id=1, name='Full Bathroom', is_boolean=True, category_id=2
    )
    Feature.objects.update_or_create(
        id=2, name='Elevator', is_boolean=True, category_id=2
    )
    Feature.objects.update_or_create(
        id=3, name='Parking', is_boolean=True, category_id=2
    )
    Feature.objects.update_or_create(
        id=4, name='Wifi', is_boolean=True, category_id=2
    )
    Feature.objects.update_or_create(
        id=5, name='Wash Machine', is_boolean=True, category_id=2
    )
    Feature.objects.update_or_create(
        id=6, name='TV', is_boolean=True, category_id=2
    )
    Feature.objects.update_or_create(
        id=7, name='Matrimonial', is_boolean=True, category_id=1
    )
    Feature.objects.update_or_create(
        id=8, name='Private Bathroom', is_boolean=True, category_id=1
    )
    Feature.objects.update_or_create(
        id=9, name='Closet', is_boolean=True, category_id=1
    )
    Feature.objects.update_or_create(
        id=10, name='Desk', is_boolean=True, category_id=1
    )
    return True


def generate_places(room, places_number):
    """Generate Places."""
    RoomPlace = apps.get_model('rooms', 'RoomPlace')
    generator = DataGenerator()
    generator = generator.fake
    for _ in range(places_number):
        category = generator.place_category()
        RoomPlace.objects.create(
            details=category.name + ' ' + generator.street_name(),
            category_id=category.id,
            room_id=room.id,
        )
    return True


def generate_favorite_rooms():
    """Generate favorites rooms."""
    FavoriteRoom = apps.get_model('rooms', 'FavoriteRoom')
    User = apps.get_model('users', 'User')
    generator = DataGenerator()
    generator = generator.fake

    for user in User.objects.filter(groups__name='Guest'):
        for room in generator.rooms(rooms_number=randint(0, 5)):
            try:
                FavoriteRoom.objects.create(user=user, room=room)
            except IntegrityError as error:
                logging.exception(error)
    return True


##
# Users
#
def create_groups():
    """Create Groups (Roles) of the users."""
    Group.objects.update_or_create(id=1, name='Host')
    Group.objects.update_or_create(id=2, name='Guest')
    return True


def create_genres():
    """Create Groups (Roles) of the users."""
    Genre = apps.get_model('users', 'Genre')
    Genre.objects.update_or_create(id=1, name='Male')
    Genre.objects.update_or_create(id=2, name='Female')
    Genre.objects.update_or_create(id=3, name='Not Specified')
    return True


def create_interests():
    """Create Interests of the users."""
    Interest = apps.get_model('users', 'Interest')
    Interest.objects.update_or_create(
        id=1,
        name='Gay Friendly',
        description='This person is friendly to gay people',
    )
    Interest.objects.update_or_create(
        id=2,
        name='Students',
        description='This person prefers students as a roommate',
    )
    Interest.objects.update_or_create(
        id=3, name='Pet Friendly', description='This person is animal friendly'
    )
    Interest.objects.update_or_create(
        id=4,
        name='No Smoking',
        description='This person prefers non-smoking roommates',
    )
    return True


def create_hobbies():
    """Create Interests of the users."""
    Hobby = apps.get_model('users', 'Hobby')
    Hobby.objects.update_or_create(
        id=1, name='Gamer', description='This person loves video games',
    )
    Hobby.objects.update_or_create(
        id=2, name='Swimming', description='This person likes to swim',
    )
    Hobby.objects.update_or_create(
        id=3, name='Basketball', description='This person practices Basketball'
    )
    Hobby.objects.update_or_create(
        id=4, name='Yoga', description='This person practices yoga',
    )
    return True


def create_profile(user, profile_data):
    """Generate user profiles."""
    Profile = apps.get_model('users', 'Profile')
    generator = DataGenerator()
    generator = generator.fake

    profile = Profile.objects.create(
        user=user,
        genre_id=generator.genre_kumpel(),
        birthday=profile_data['birthdate'],
    )
    profile.hobbies.set(generator.hobbies(hobbies_number=randint(1, 4)))
    profile.interests.set(generator.interest(interests_number=randint(1, 4)))

    try:
        response = requests.get(
            'https://source.unsplash.com/1600x900/?profile'
        )

        avatar = Image.open(BytesIO(response.content)).convert('RGB')

        avatar_io = BytesIO()

        avatar.save(avatar_io, format='JPEG', quality=70)

        thumb_file = ContentFile(avatar_io.getvalue())

        profile.picture.save(
            os.path.basename(user.username + '.jpg'), thumb_file
        )
    except requests.exceptions.ConnectionError as error:
        logging.exception(error)
    date = generator.date_between(
        start_date=datetime.date(2020, 1, 1), end_date='today'
    )
    user.created = date
    user.date_joined = date
    profile.created = date
    user.modified = date
    profile.modified = date
    user.save()
    profile.save()
    return True


def create_users(n):
    """Generate users."""
    User = apps.get_model('users', 'User')
    generator = DataGenerator()
    generator = generator.fake
    for _ in range(n):
        profile_data = generator.profile()
        name = profile_data['name'].split()
        first_name = name[0]
        last_name = ' '.join(name[1:])
        username = (
            first_name.lower()
            + '_'
            + last_name.lower()
            + str(generator.random_number(digits=4))
        )
        user_obj = User(
            email=username + '@hotmail.com',
            first_name=first_name,
            last_name=last_name,
            phone_number=generator.bothify(text='+##############'),
            username=username,
            is_verified=True,
            is_client=True,
        )
        user_obj.set_password('njEL@5%DcAxN3*#')
        user_obj.save()
        user_obj.groups.add(generator.group())
        create_profile(user_obj, profile_data)
    return True
