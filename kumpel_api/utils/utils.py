"""Paginator utility for schemas."""
# pylint: disable=R0801
import os
from io import BytesIO
from random import choice
from random import sample

from django.contrib.auth.models import Group
from django.core.files.base import ContentFile
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger
from django.core.paginator import Paginator
from faker import Faker
from faker.providers import BaseProvider
from PIL import Image

from kumpel_api.rooms.models import RoomImage
from kumpel_api.rooms.models import Status
from kumpel_api.rooms.models.feature import Feature
from kumpel_api.rooms.models.feature_category import FeatureCategory
from kumpel_api.rooms.models.places_category import PlacesCategory
from kumpel_api.rooms.models.rooms import Room
from kumpel_api.users.models import Genre
from kumpel_api.users.models import Hobby
from kumpel_api.users.models import Interest
from kumpel_api.users.models import Profile
from kumpel_api.users.models import User


def reduce_avatars():
    """Reduce file size of the user avatars."""
    profiles = Profile.objects.all()

    for profile in profiles:
        image = Image.open(profile.picture)
        # create a BytesIO object
        image_io = BytesIO()
        # save image to BytesIO object
        image.save(image_io, 'JPEG', quality=70)
        # create a django-friendly Files object
        thumb_file = ContentFile(image_io.getvalue())

        profile.picture.save(
            os.path.basename(profile.user.username + '.jpg'), thumb_file
        )

        profile.save()


def reduce_rooms():
    """Reduce file size of rooms."""
    rooms = Room.objects.all()

    # pylint: disable=undefined-loop-variable

    for room in rooms:
        image = Image.open(room.main_image)
        # create a BytesIO object
        image_io = BytesIO()
        # save image to BytesIO object
        image.save(image_io, 'JPEG', quality=70)
        # create a django-friendly Files object
        thumb_file = ContentFile(image_io.getvalue())

        room.main_image.save(os.path.basename(room.title + '.jpg'), thumb_file)

        room.save()

    for room_image in RoomImage.objects.all():
        image = Image.open(room_image.image)
        # create a BytesIO object
        image_io = BytesIO()
        # save image to BytesIO object
        image.save(image_io, 'JPEG', quality=70)
        # create a django-friendly Files object
        thumb_file = ContentFile(image_io.getvalue())

        room_image.image.save(
            os.path.basename(room_image.room.title + '.jpg'), thumb_file
        )

        room.save()


class UserProvider(BaseProvider):
    """Provider users id."""

    # pylint: disable=too-few-public-methods

    users = User.objects.all().values_list('id', flat=True)

    def user(self):
        """Return a random user id."""
        if len(self.users) > 0:
            return choice(self.users)

        return None


class GenreProvider(BaseProvider):
    """Provider profiles id."""

    # pylint: disable=too-few-public-methods

    genres = Genre.objects.all().values_list('id', flat=True)

    def genre_kumpel(self):
        """Return a random genre id."""
        if len(self.genres) > 0:
            return choice(self.genres)

        return None


class HobbyProvider(BaseProvider):
    """Provider Hobbies id."""

    # pylint: disable=too-few-public-methods

    hobbies_list = Hobby.objects.all()

    def hobbies(self, hobbies_number=3):
        """Return a random hobby id."""
        if len(self.hobbies_list) > 0:
            return sample(set(self.hobbies_list), hobbies_number)

        return None


class InterestProvider(BaseProvider):
    """Provider Intrests id."""

    # pylint: disable=too-few-public-methods

    interests = Interest.objects.all()

    def interest(self, interests_number=3):
        """Return a random interest id."""
        if len(self.interests) > 0:
            return sample(set(self.interests), interests_number)

        return None


class ProfileProvider(BaseProvider):
    """Provider Profiles id."""

    # pylint: disable=too-few-public-methods

    profiles = Profile.objects.all().values_list('id', flat=True)

    def kumpel_profile(self):
        """Return a random interest id."""
        if len(self.profiles) > 0:
            return choice(self.profiles)

        return None


class StatusProvider(BaseProvider):
    """Provider Status id."""

    # pylint: disable=too-few-public-methods

    status_list = Status.objects.all().values_list('id', flat=True)

    def status(self):
        """Return a random interest id."""
        if len(self.status_list) > 0:
            return choice(self.status_list)

        return None


class FeatureCategoryProvider(BaseProvider):
    """Provider Feature Category id."""

    # pylint: disable=too-few-public-methods

    feature_categories = FeatureCategory.objects.all().values_list(
        'id', flat=True
    )

    def feature_category(self):
        """Return a random interest id."""
        if len(self.feature_categories) > 0:
            return choice(self.feature_categories)

        return None


class FeatureProvider(BaseProvider):
    """Provider Feature."""

    # pylint: disable=too-few-public-methods

    features_list = Feature.objects.all()

    def features(self, features_number):
        """Return a random feature."""
        if len(self.features_list) > 0:
            return sample(set(self.features_list), features_number)

        return None


class PlacesCategoryProvider(BaseProvider):
    """Provider a Category."""

    # pylint: disable=too-few-public-methods

    places_category = PlacesCategory.objects.all()

    def place_category(self):
        """Return a random place category."""
        if len(self.places_category) > 0:
            return choice(self.places_category)

        return None


class RoomProvider(BaseProvider):
    """Provider a Room Id."""

    # pylint: disable=too-few-public-methods

    rooms_list = Room.objects.all().values_list('id', flat=True)
    rooms_obj = Room.objects.all()

    def room(self):
        """Return a random room."""
        if len(self.rooms_list) > 0:
            return choice(self.rooms_list)

        return None

    def rooms(self, rooms_number):
        """Return a random room."""
        if len(self.rooms_obj) > 0:
            rooms = sample(set(self.rooms_obj), rooms_number)
            return rooms

        return None


class GroupsProvider(BaseProvider):
    """Provider a Group."""

    # pylint: disable=too-few-public-methods

    groups_list = Group.objects.exclude(name='Spartans')

    def group(self):
        """Return a random group."""
        if len(self.groups_list) > 0:
            return choice(self.groups_list)

        return None


class DataGenerator(object):
    """Singleton data generator."""

    # pylint: disable=useless-object-inheritance,too-few-public-methods

    class __DataGenerator:
        """Data generator."""

        # pylint: disable=C0103

        def __init__(self):
            """Initialize Data Generator."""
            self.fake = Faker()
            Faker.seed(0)
            self.fake.add_provider(UserProvider)
            self.fake.add_provider(GenreProvider)
            self.fake.add_provider(HobbyProvider)
            self.fake.add_provider(InterestProvider)
            self.fake.add_provider(ProfileProvider)
            self.fake.add_provider(StatusProvider)
            self.fake.add_provider(FeatureCategoryProvider)
            self.fake.add_provider(FeatureProvider)
            self.fake.add_provider(PlacesCategoryProvider)
            self.fake.add_provider(RoomProvider)
            self.fake.add_provider(GroupsProvider)

        def __str__(self):
            """Return fake name."""
            return self + ' ' + self.fake

    instance = None

    def __new__(cls):
        """Return instace of singleton DataGenerator."""
        if not DataGenerator.instance:
            DataGenerator.instance = DataGenerator.__DataGenerator()
        return DataGenerator.instance

    def __getattr__(self, fake):
        """Return fake data generator."""
        return getattr(self.instance, fake)


def get_paginator(rooms, page_size, page, paginated_type, **kwargs):
    """Paginator template for querys.

    Author: Elias Ojeda - Eocode

    Args:
        rooms (list): rooms list
        page_size (int): page size
        page (int): page
        paginated_type ([type]): [description]

    Returns:
        [type]: [description]
    """
    pag = Paginator(rooms, page_size)
    try:
        page_obj = pag.page(page)
    except PageNotAnInteger:
        page_obj = pag.page(1)
    except EmptyPage:
        page_obj = pag.page(pag.num_pages)
    return paginated_type(
        page=page_obj.number,
        pages=pag.num_pages,
        has_next=page_obj.has_next(),
        has_prev=page_obj.has_previous(),
        data=page_obj.object_list,
        **kwargs
    )
