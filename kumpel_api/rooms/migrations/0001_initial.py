# Generated by Django 3.0.7 on 2020-07-08 04:39

import colorfield.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Feature',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, help_text='Date time on which the object was created.', verbose_name='created at')),
                ('modified', models.DateTimeField(auto_now=True, help_text='Date time on which the object was last modified.', verbose_name='modified at')),
                ('name', models.CharField(help_text='Feature Name', max_length=40, unique=True)),
                ('icon', models.CharField(help_text='String for icons in the application', max_length=50)),
                ('is_boolean', models.BooleanField(default=True, help_text='Identify Booleans Features')),
                ('description', models.CharField(blank=True, help_text='Feature Description', max_length=200, null=True)),
            ],
            options={
                'verbose_name_plural': 'Features',
                'db_table': 'FEATURES',
                'ordering': ['-created', '-modified'],
                'get_latest_by': 'created',
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='FeatureCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, help_text='Date time on which the object was created.', verbose_name='created at')),
                ('modified', models.DateTimeField(auto_now=True, help_text='Date time on which the object was last modified.', verbose_name='modified at')),
                ('name', models.CharField(help_text='Category Name', max_length=40)),
                ('description', models.CharField(blank=True, help_text='Category Description', max_length=200, null=True)),
            ],
            options={
                'verbose_name_plural': 'Feature Categories',
                'db_table': 'FEATURE_CATEGORIES',
                'ordering': ['-created', '-modified'],
                'get_latest_by': 'created',
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PlacesCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, help_text='Date time on which the object was created.', verbose_name='created at')),
                ('modified', models.DateTimeField(auto_now=True, help_text='Date time on which the object was last modified.', verbose_name='modified at')),
                ('name', models.CharField(help_text='Name of category for places near to Room', max_length=60)),
                ('description', models.CharField(blank=True, help_text='Places Category description', max_length=500, null=True)),
            ],
            options={
                'verbose_name_plural': 'Places Categories',
                'db_table': 'PLACES_CATEGORIES',
                'ordering': ['-created', '-modified'],
                'get_latest_by': 'created',
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, help_text='Date time on which the object was created.', verbose_name='created at')),
                ('modified', models.DateTimeField(auto_now=True, help_text='Date time on which the object was last modified.', verbose_name='modified at')),
                ('title', models.CharField(help_text='Room Title', max_length=80, verbose_name='title')),
                ('description', models.CharField(help_text='Room Description', max_length=200)),
                ('main_image', models.ImageField(blank=True, help_text='Main Image of the Room', null=True, upload_to='rooms/images/', verbose_name='main image')),
                ('price', models.FloatField(blank=True, default=0, help_text='Price to rent room')),
                ('address', models.CharField(help_text='Room Address', max_length=40)),
                ('latitude', models.CharField(blank=True, help_text='Room latitude', max_length=20, null=True)),
                ('longitude', models.CharField(blank=True, help_text='Room longitude', max_length=20, null=True)),
                ('visibility', models.BooleanField(default=False, help_text='Room visiblity for all users')),
                ('availability_date', models.DateTimeField(blank=True, help_text='Availability Room. Date Field Format: YYYY-MM-DD HH:MM[:ss[.uuuuuu]', null=True)),
            ],
            options={
                'db_table': 'ROOMS',
                'ordering': ['-created', '-modified'],
                'get_latest_by': 'created',
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, help_text='Date time on which the object was created.', verbose_name='created at')),
                ('modified', models.DateTimeField(auto_now=True, help_text='Date time on which the object was last modified.', verbose_name='modified at')),
                ('status', models.CharField(help_text='Rooms status', max_length=30, unique=True)),
                ('icon', models.CharField(blank=True, help_text='Icon for the Status', max_length=100, null=True)),
                ('color', colorfield.fields.ColorField(blank=True, default='#FF0000', help_text='Personalized Color Type.', max_length=18, null=True, verbose_name='color')),
            ],
            options={
                'verbose_name_plural': 'Status',
                'db_table': 'STATUS',
                'ordering': ['-created', '-modified'],
                'get_latest_by': 'created',
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RoomPlace',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, help_text='Date time on which the object was created.', verbose_name='created at')),
                ('modified', models.DateTimeField(auto_now=True, help_text='Date time on which the object was last modified.', verbose_name='modified at')),
                ('name', models.CharField(help_text='Name of the place near to Room', max_length=60)),
                ('details', models.CharField(blank=True, help_text='Place description', max_length=500, null=True)),
                ('category', models.ForeignKey(default=1, help_text='Category of the place', on_delete=django.db.models.deletion.SET_DEFAULT, to='rooms.PlacesCategory')),
                ('room', models.ForeignKey(help_text='Room near of the place', on_delete=django.db.models.deletion.CASCADE, to='rooms.Room')),
            ],
            options={
                'verbose_name_plural': 'Rooms Places',
                'db_table': 'ROOMS_PLACES',
                'ordering': ['-created', '-modified'],
                'get_latest_by': 'created',
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RoomImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, help_text='Date time on which the object was created.', verbose_name='created at')),
                ('modified', models.DateTimeField(auto_now=True, help_text='Date time on which the object was last modified.', verbose_name='modified at')),
                ('image', models.ImageField(help_text='Room Image', upload_to='rooms/images')),
                ('room', models.ForeignKey(help_text='Room of the Image', on_delete=django.db.models.deletion.CASCADE, to='rooms.Room')),
            ],
            options={
                'verbose_name_plural': 'Rooms Images',
                'db_table': 'ROOMS_IMAGES',
                'ordering': ['-created', '-modified'],
                'get_latest_by': 'created',
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RoomFeature',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, help_text='Date time on which the object was created.', verbose_name='created at')),
                ('modified', models.DateTimeField(auto_now=True, help_text='Date time on which the object was last modified.', verbose_name='modified at')),
                ('number', models.FloatField(blank=True, default=0, help_text='Number for Features with measures example: room_size, number of television', null=True)),
                ('feature', models.ForeignKey(help_text='Feature of the room', on_delete=models.SET(1), to='rooms.Feature')),
                ('room', models.ForeignKey(help_text='Room that have the feature', on_delete=django.db.models.deletion.CASCADE, to='rooms.Room')),
            ],
            options={
                'verbose_name_plural': 'Rooms Features',
                'db_table': 'ROOMS_FEATURES',
                'ordering': ['-created', '-modified'],
                'get_latest_by': 'created',
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='room',
            name='features',
            field=models.ManyToManyField(help_text='Room Features', through='rooms.RoomFeature', to='rooms.Feature'),
        ),
        migrations.AddField(
            model_name='room',
            name='host',
            field=models.ForeignKey(help_text='Room Owner', on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='room',
            name='status',
            field=models.ForeignKey(help_text='Status Room', on_delete=django.db.models.deletion.PROTECT, to='rooms.Status'),
        ),
        migrations.AddField(
            model_name='feature',
            name='category',
            field=models.ForeignKey(help_text='Feature Category', null=True, on_delete=django.db.models.deletion.PROTECT, to='rooms.FeatureCategory'),
        ),
        migrations.CreateModel(
            name='FavoriteRoom',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, help_text='Date time on which the object was created.', verbose_name='created at')),
                ('modified', models.DateTimeField(auto_now=True, help_text='Date time on which the object was last modified.', verbose_name='modified at')),
                ('room', models.ForeignKey(help_text='Room that a user add to favorite', on_delete=django.db.models.deletion.CASCADE, to='rooms.Room')),
                ('user', models.ForeignKey(help_text='User that add the room to favorite', on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'FAVORITE_ROOMS',
                'ordering': ['-created', '-modified'],
                'get_latest_by': 'created',
                'abstract': False,
            },
        ),
    ]
