"""Places Category Model."""
from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE

from kumpel_api.utils.models import KumpelBaseModel


class PlacesCategory(KumpelBaseModel, SafeDeleteModel):
    """Places Category model."""

    # pylint: disable=too-few-public-methods

    _safedelete_policy = SOFT_DELETE

    name = models.CharField(
        max_length=60,
        null=False,
        blank=False,
        help_text='Name of category for places near to Room',
    )

    description = models.CharField(
        max_length=500,
        null=True,
        blank=True,
        help_text='Places Category description',
    )

    def __str__(self):
        """Return name of the place."""
        return self.name

    class Meta(KumpelBaseModel.Meta):
        """Meta options for RoomPlaces."""

        # pylint: disable=too-few-public-methods

        db_table = 'PLACES_CATEGORIES'
        verbose_name_plural = 'Places Categories'
