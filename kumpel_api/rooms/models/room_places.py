"""Rooms Places Model for Kumpel API."""
from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE

from kumpel_api.rooms.models import Room
from kumpel_api.rooms.models.places_category import PlacesCategory
from kumpel_api.utils.models import KumpelBaseModel


class RoomPlace(KumpelBaseModel, SafeDeleteModel):
    """Room Places model."""

    # pylint: disable=too-few-public-methods

    _safedelete_policy = SOFT_DELETE

    # pylint: disable=R0801
    room = models.ForeignKey(
        Room,
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        help_text='Room near of the place',
    )

    details = models.CharField(
        max_length=500, null=True, blank=True, help_text='Place description'
    )

    category = models.ForeignKey(
        PlacesCategory,
        default=1,
        null=False,
        blank=False,
        help_text='Category of the place',
        on_delete=models.SET_DEFAULT,
    )

    def __str__(self):
        """Return name of the place."""
        return self.details

    class Meta(KumpelBaseModel.Meta):
        """Meta options for RoomPlaces."""

        # pylint: disable=too-few-public-methods

        db_table = 'ROOMS_PLACES'
        verbose_name_plural = 'Rooms Places'
