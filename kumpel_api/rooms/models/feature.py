"""Features Model for Kumpel API."""
from django.core.validators import FileExtensionValidator
from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE

from kumpel_api.rooms.models.feature_category import FeatureCategory
from kumpel_api.utils.models import KumpelBaseModel


class Feature(KumpelBaseModel, SafeDeleteModel):
    """Feature of a Room."""

    # pylint: disable=too-few-public-methods

    _safedelete_policy = SOFT_DELETE

    name = models.CharField(
        unique=True,
        max_length=40,
        blank=False,
        null=False,
        help_text='Feature Name',
    )

    icon = models.FileField(
        upload_to='rooms/features/',
        blank=False,
        null=False,
        help_text='Image icon for features in the application',
        validators=[FileExtensionValidator(['svg'])],
    )

    is_boolean = models.BooleanField(
        default=True,
        null=False,
        blank=False,
        help_text='Identify Booleans Features',
    )

    description = models.CharField(
        max_length=200, blank=True, null=True, help_text='Feature Description'
    )

    category = models.ForeignKey(
        FeatureCategory,
        on_delete=models.PROTECT,
        null=True,
        help_text='Feature Category',
    )

    def __str__(self):
        """Return name of the feature."""
        return self.name

    class Meta(KumpelBaseModel.Meta):
        """Meta options for Feature Model."""

        # pylint: disable=too-few-public-methods

        db_table = 'FEATURES'
        verbose_name_plural = 'Features'
