"""Rooms images model."""
from django.conf import settings
from django.db import models
from django.utils.safestring import mark_safe
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE

from kumpel_api.rooms.models import Room
from kumpel_api.utils.models import KumpelBaseModel


class RoomImage(KumpelBaseModel, SafeDeleteModel):
    """Images of rooms."""

    _safedelete_policy = SOFT_DELETE

    image = models.ImageField(
        upload_to='rooms/images',
        blank=False,
        null=False,
        help_text='Room Image',
    )
    room = models.ForeignKey(
        Room, on_delete=models.CASCADE, help_text='Room of the Image'
    )

    def __str__(self):
        """Return image path."""
        return str(self.image)

    def image_tag(self):
        """Create HTML picture user for display on apps."""
        if self.image:
            return mark_safe(  # nosec - ✅ show image on admin
                f'<img src="{settings.MEDIA_URL}{self.image}" style="width: 50px;" />'
            )
        return 'Image not found'

    class Meta(KumpelBaseModel.Meta):
        """Meta options for Rooms Images."""

        # pylint: disable=too-few-public-methods
        db_table = 'ROOMS_IMAGES'
        verbose_name_plural = 'Rooms Images'
