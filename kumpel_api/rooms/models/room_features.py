"""Rooms Features model."""
from django.db import models

from kumpel_api.rooms.models import Room
from kumpel_api.rooms.models.feature import Feature
from kumpel_api.utils.models import KumpelBaseModel


class RoomFeature(KumpelBaseModel):
    """Room Feature through entity."""

    # pylint: disable=too-few-public-methods

    room = models.ForeignKey(
        Room, on_delete=models.CASCADE, help_text='Room that have the feature'
    )

    feature = models.ForeignKey(
        Feature, on_delete=models.SET(1), help_text='Feature of the room'
    )

    number = models.FloatField(
        blank=True,
        null=True,
        default=0,
        help_text='Number for Features with measures example: room_size, number of television',
    )

    class Meta(KumpelBaseModel.Meta):
        """Meta options for RoomFeature."""

        db_table = 'ROOMS_FEATURES'
        verbose_name_plural = 'Rooms Features'
