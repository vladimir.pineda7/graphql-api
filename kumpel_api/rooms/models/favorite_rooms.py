"""Favorite Rooms Model."""
from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE

from kumpel_api.rooms.models import Room
from kumpel_api.users.models import User
from kumpel_api.utils.models import KumpelBaseModel


class FavoriteRoom(KumpelBaseModel, SafeDeleteModel):
    """Favorites Rooms of the users."""

    _safedelete_policy = SOFT_DELETE

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        null=False,
        help_text='User that add the room to favorite',
    )

    # pylint: disable=R0801

    room = models.ForeignKey(
        Room,
        on_delete=models.CASCADE,
        null=False,
        help_text='Room that a user add to favorite',
    )

    def get_user_full_name(self):
        """Return fullname of the user that add the room to favorites."""
        return self.user.get_full_name()

    def get_room_title(self):
        """Return the title of the room."""
        return self.room.title

    class Meta(KumpelBaseModel.Meta):
        """Meta options for Favorite Rooms."""

        # pylint: disable=too-few-public-methods
        db_table = 'FAVORITE_ROOMS'
