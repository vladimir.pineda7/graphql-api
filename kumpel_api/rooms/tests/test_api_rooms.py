"""API Rooms Tests."""
from graphene_django.utils.testing import GraphQLTestCase

from core.schema import schema
from kumpel_api.rooms.models.rooms import Room
from kumpel_api.rooms.models.status import Status
from kumpel_api.users.models.users import User


# pylint: disable=too-few-public-methods


class RoomMutationTestCase(GraphQLTestCase):
    """User Tests API."""

    GRAPHQL_SCHEMA = schema
    GRAPHQL_URL = '/graphql'
    # pylint: disable=invalid-name

    def setUp(self):  # noqa: D401
        """Setup for tests."""
        self.user = User.objects.create(
            email='test_user@kumpel.net',
            first_name='Test',
            last_name='User',
            phone_number='+573204589622',
            password='password',
        )

        self.room_status = Status.objects.create(status='Underpass')

        self.room = Room.objects.create(
            host_id=self.user.id,
            title='Test Title Room',
            description='Room Test Description',
            address='Address test room',
            status_id=self._room_status.id,
        )


# def test_work_create_room_query(self):
#     """Verify that query for create a room work."""
#     response = self.query(
#         """
#         mutation ($images: Upload!){
#         roomCreate(
#             title: "Condesa room",
#             address: "Condesa n5",
#             description: "A simple room",
#             hostId: 1, statusId:1,
#             images: $images,
#             visibility:true
#         ){
#             room {
#             id
#             title
#             description
#             mainImage
#             }
#         }
#         }
#         """
#     )
#     self.assertResponseNoErrors(response)


def test_delete_room_mutation_works(self):
    """Verify that mutation for delete a rooms works."""
    response = self.query(
        f"""
        mutation{{
            roomDelete(roomId:{self.room.id}) {{
                room{{
                    title
                }}
            }}
        }}
        """
    )
    self.assertResponseNoErrors(response)


def test_undelete_room_mutation_works(self):
    """Verify that mutation for undelete a rooms works."""
    self.room.delete()
    response = self.query(
        f"""
        mutation{{
            roomUndelete(roomId:{self.room.id}) {{
                room{{
                    title
                }}
            }}
        }}
        """
    )
    self.assertResponseNoErrors(response)


class RoomQueryTestCase(GraphQLTestCase):
    """Profile Tests API."""

    GRAPHQL_SCHEMA = schema
    GRAPHQL_URL = '/graphql'

    def test_search_room_query(self):
        """Verify that query for search rooms works."""
        response = self.query(
            """
            query {
                roomsSearch(textToSearch:"test", page: 1, limit: 5){
                    data{
                        id
                        title
                        description
                    }
                }
            }
            """
        )
        self.assertResponseNoErrors(response)
